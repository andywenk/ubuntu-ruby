# ubuntu-ruby

My notes on how to get ruby and a Sinatra application running on Ubuntu 20.

## Cloud

I used different cloud hosting providers and I always had trouble to get everything working. That was [Heroku](https://heroku.com/), [render](https://render.com/), [Digital Ocean](https://www.digitalocean.com/) and [Ampliify on AWS](https://aws.amazon.com/de/amplify/). All these services have advantages but one needs to read a thousand tutorials how to get shit working. As I am a long year user of [Hetzner](https://www.hetzner.de) for domain registration and DNS, I decided to give the [Hetzner Cloud](https://www.hetzner.com/cloud) a try. 

It is nice to have a service provider with mechanisms to set up a box with the stuff you need. Heroku has also a very nice [CLI](https://en.wikipedia.org//wiki/Command-line_interface) to manage everything. But as I am developing software for the web since over 25 years I am still well known how to install a fresh Linux box and the stuff I need there. That was the way we did it in the early days. *"Yeah you are an old guy - nowadays we do it differently."*. Maybe. But I am 100% sure you simply pay money to service providers you could spare and use it for other stuff. Furthermore I expect you to understand the basic principles what a Linux box serving an application via a webserver is doing. And last but not least the overhead you can expect with a system like Heroku, Digital Ocean or AWS is in 99% not comparable to what you get as a result. You are not likely to run Facebook or Twitter. Maybe you have a bigger setup with 10 to 20 boxes. Well - dig into [Ansible](https://www.ansible.com/community) or a tool like that and you will also be good to go with many boxes you need to provision.
 
I have to say - I am very satisfied with the [Hetzner Cloud](https://www.hetzner.com/cloud). It has a very good and intuitive Web-UI, it is easy to use, it is cheaper compared to the other providers and what you get for your money is very much more regarding the boxes in the cloud. And finally the performancde is way better. I leave the comparison up to you.

## The Box

I provisioned a Ubuntu 20 box with these features:

* Name: CX21
* 2 VCPU
* 4 GBRAM
* 40 GB DISK LOKAL
* 20 TB TRAFFIC OUT
* 5,77 EUR / month

## Basic setup

Create a SSH key pair (hint: use 1Password to store the key) and upload it to the Web-UI. You will be able to ssh as root into the box. You can find the IP on the first page of the dashboard.

### Update Ubuntu

	~ ssh root@111.222.333.444
	$ apt-get update
	$ apt-get upgrade
	
### Make vim standard editor

	$ sudo update-alternatives --config editor
	There are 4 choices for the alternative editor (providing /usr/bin/editor).
	
	Selection	 Path				  Priority  Status
	----------------------------------------------------
	* 0          /bin/nano            40        auto mode
  	1            /bin/ed             -100       manual mode
  	2            /bin/nano            40        manual mode
  	3            /usr/bin/vim.basic   30        manual mode
  	4            /usr/bin/vim.tiny    15        manual mode

	Press <enter> to keep the current choice[*], or type selection number: 3
	update-alternatives: using /usr/bin/vim.basic to provide /usr/bin/editor (editor) in manual mode

### Set root password

	$ passwd root
	
### Add a new user with root privs and define ssh key to login

	$ adduser harry
	[...]
	$ visudo
	harry ALL=(ALL:ALL)ALL 			<-- add this after root
	$ su - harry
	harry@ubuntu-4gb-fsn1-1:~$ ll
	total 20
	drwxr-xr-x 2 harry harry 4096 Oct 29 12:22 ./
	drwxr-xr-x 3 root root 4096 Oct 29 12:22 ../
	-rw-r--r-- 1 harry harry  220 Oct 29 12:22 .bash_logout
	-rw-r--r-- 1 harry harry 3771 Oct 29 12:22 .bashrc
	-rw-r--r-- 1 harry harry    0 Oct 29 12:22 .cloud-locale-test.skip
	-rw-r--r-- 1 harry harry  807 Oct 29 12:22 .profile
	$ exit
	~ ssh-copy-id -i ~/.ssh/your_rsa.pub harry@111.222.333.444
	/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/Users/harry/.ssh/your_rsa.pub"
	/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
	/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
	harry@111.222.333.444's password:
	
	Number of key(s) added:        1
	
	Now try logging into the machine, with:   "ssh 'harry@111.222.333.444'"
	and check to make sure that only the key(s) you wanted were added.
	~ ssh harry@111.222.333.444
	
That should work now.

### Remove root login

	$ sudo vi /etc/ssh/sshd_config

Change

	PermitRootLogin yes

to

	PermitRootLogin no
	
Then

	$ /etc/init.d/ssh restart				<-- needs root password (see above)
	
## Enable firewall ufw

	$ sudo ufw enable
	$ sudo ufw app list
	Available applications:
	  OpenSSH
	
## Install Ruby

	$ sudo apt install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm6 	libgdbm-dev libdb-dev
	$ curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash
	$ ~/.rbenv/bin/rbenv init
	# Please add the following line to your `~/.bashrc' file,
	# then restart your terminal.
	
	eval "$(/home/harry/.rbenv/bin/rbenv init - bash)"
	
	$ vi ~ /.bashrc
	$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
	$ echo 'eval "$(rbenv init -)"' >> ~/.bashrc
	$ source ~/.bashrc
	$  rbenv install -l
	2.6.10
	2.7.6
	3.0.4
	3.1.2
	jruby-9.3.9.0
	mruby-3.1.0
	picoruby-3.0.0
	rbx-5.0
	truffleruby-22.3.0
	truffleruby+graalvm-22.3.0
	
	Only latest stable releases for each Ruby implementation are shown.
	Use 'rbenv install --list-all / -L' to show all local versions.
	
	$ rbenv install 3.1.2
	$ rbenv global 3.1.2
	
## Install nginx

nginx is maybe the best webserver you can get today. Apache ist still very good and well known though.

	$ sudo apt-get install nginx
	$ sudo service nginx start
	$ sudo service nginx status
	● nginx.service - A high performance web server and a reverse proxy server
	     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
	     Active: active (running) since Sat 2022-10-29 13:59:15 UTC; 1min 3s ago
	       Docs: man:nginx(8)
	   Main PID: 47019 (nginx)
	      Tasks: 3 (limit: 4556)
	     Memory: 4.5M
	     CGroup: /system.slice/nginx.service
	             ├─47019 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
	             ├─47020 nginx: worker process
	             └─47021 nginx: worker process
	
	Oct 29 13:59:15 ubuntu-4gb-fsn1-1 systemd[1]: Starting A high performance web server and a reverse proxy server...
	Oct 29 13:59:15 ubuntu-4gb-fsn1-1 systemd[1]: Started A high performance web server and a reverse proxy server.
	
Update the firewall settings:

	$ sudo ufw allow 'nginx http'
	Rule added
	Rule added (v6)
	$ sudo ufw app list
	Available applications:
	  Nginx Full
	  Nginx HTTP
	  Nginx HTTPS
	  OpenSSH	
	$ sudo ufw reload
	Firewall reloaded
	
Now everything should work and nginx is running. 

### Correct permissions for `/var/www` with Capistrano

Make sure to set the permissons for any directories in `/var/www` when you want Capistrano to be able to create directories there. When the user to deploy the application is e.g. `harry` you should change the directories ownership in `/var/www/your-application-deployed-by-capistrano` to 

	$ sudo chown -R harry:www-data /var/www/your-application-deployed-by-capistrano
	
## Install Passenger

This is kindly stolen from ["Installing Passenger"](https://www.phusionpassenger.com/docs/advanced_guides/install_and_upgrade/nginx/install/oss/focal.html)

	$ sudo apt-get install -y dirmngr gnupg apt-transport-https ca-certificates 
	$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
	$ sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger focal main > /etc/apt/sources.list.d/passenger.list'
	$ sudo apt-get update
	$ sudo apt-get install -y libnginx-mod-http-passenger
	$ if [ ! -f /etc/nginx/modules-enabled/50-mod-http-passenger.conf ]; then sudo ln -s /usr/share/nginx/modules-available/mod-http-passenger.load /etc/	nginx/modules-enabled/50-mod-http-passenger.conf ; fi
	$ sudo ls /etc/nginx/conf.d/mod-http-passenger.conf
	$ sudo service nginx restart
	$ sudo /usr/bin/passenger-config validate-install
	What would you like to validate?
	Use <space> to select.
	If the menu doesn't display correctly, press '!'
	
	 ‣ ⬢  Passenger itself
	   ⬡  Apache

	-------------------------------------------------------------------------
	
	 * Checking whether this Passenger install is in PATH... ✓
	 * Checking whether there are no other Passenger installations... ✓
	
	Everything looks good. :-)	
	
Check the stats with

	$ sudo /usr/sbin/passenger-memory-stats

### Important config

As we are using rbenv you need to configure the correct ruby for passenger

	$ vi /etc/nginx/conf.d/mod-http-passenger.conf
	passenger_ruby /home/harry/.rbenv/shims/ruby;

## Install PostgreSQL as the production database

	$ sudo apt install postgresql postgresql-contrib libpq-dev
	$ sudo service postgresql start
	$ sudo -i -u postgres
	$ psql
	$ psql
	psql (12.12 (Ubuntu 12.12-0ubuntu0.20.04.1))
	Type "help" for help.
	
	postgres=# \q
	$ createuser --interactive
	Enter name of role to add: krx
	Shall the new role be a superuser? (y/n) n
	Shall the new role be allowed to create databases? (y/n) y
	Shall the new role be allowed to create more new roles? (y/n) n
	
You need to change the way your application is connecting to the database. 
	
	$ vi /etc/postgresql/12/main/pg_hba.conf

Change the line

	local   all             all                                     peer

to

	local   all             all                                     password
		
## Add capsitrano to the application

[Capistrano](https://capistranorb.com/) is a very powerful and simple deployment tool for Ruby or Rack applications. 

### Add capistrano to the application

Add these gems to your `Gemfile`:
	
	gem "capistrano"
	gem "capistrano-bundler"
	gem "capistrano-passenger"
	gem "capistrano-rbenv"
	gem "ed25519"
	gem "bcrypt_pbkdf"

Run

	~ bundle install
	
Important note on capistrano-passenger
	
	Post-install message from capistrano-passenger:
	==== Release notes for capistrano-passenger ====
	passenger once had only one way to restart: `touch tmp/restart.txt`
	Beginning with passenger v4.0.33, a new way was introduced: `passenger-config restart-app`
	
	The new way to restart was not initially practical for everyone,
	since for versions of passenger prior to v5.0.10,
	it required your deployment user to have sudo access for some server configurations.
	
	capistrano-passenger gives you the flexibility to choose your restart approach, or to rely on reasonable defaults.
	
	If you want to restart using `touch tmp/restart.txt`, add this to your config/deploy.rb:
	
	    set :passenger_restart_with_touch, true
	
	If you want to restart using `passenger-config restart-app`, add this to your config/deploy.rb:
	
	    set :passenger_restart_with_touch, false # Note that `nil` is NOT the same as `false` here
	
	If you don't set `:passenger_restart_with_touch`, capistrano-passenger will check what version of passenger you are running
	and use `passenger-config restart-app` if it is available in that version.
	
	If you are running passenger in standalone mode, it is possible for you to put passenger in your
	Gemfile and rely on capistrano-bundler to install it with the rest of your bundle.
	If you are installing passenger during your deployment AND you want to restart using `passenger-config restart-app`,
	you need to set `:passenger_in_gemfile` to `true` in your `config/deploy.rb`.
	================================================
	
Now setup capistrano in your app

	~ bundle exec cap install
	mkdir -p config/deploy
	create config/deploy.rb
	create config/deploy/staging.rb
	create config/deploy/production.rb
	mkdir -p lib/capistrano/tasks
	create Capfile
	Capified

Now you need to uncomment some requirements in the `Capfile` in the root of your application:

	require "capistrano/rbenv"
	require "capistrano/bundler"
	require "capistrano/passenger"

Before we continue we set up the directory on the webserver and set the owner to the standard nginx user `www-data`

	$ sudo mkdir /var/www/krx-html-shortener
	$ sudo chown -R www-data:www-data /var/www/krx-html-shortener
	
Edit now the Capistrano deply settings for your application in `config/deploy`in your application.

	set :application, "krx-url-shortener"
	set :repo_url, "git@codeberg.org:andywenk/url-shortener.git"
	set :deploy_to, "/var/www/krx-url-shortener"
	set :ssh_options, { :forward_agent => true }
	
Now let's try the first deployment

	~ bundle exec cap production deploy
	
Most probably it will work and will a structure onb the webserver like this:

	~ harry@ubuntu-4gb-fsn1-1:/var/www/krx-url-shortener$ ll
	total 20
	drwxr-xr-x 5 harry www-data 	4096 Oct 29 14:36 ./
	drwxr-xr-x 4 root root     	4096 Oct 29 14:07 ../
	drwxrwxr-x 3 harry harry     	4096 Oct 29 14:36 releases/
	drwxrwxr-x 7 harry harry     	4096 Oct 29 14:36 repo/
	drwxrwxr-x 3 harry harry     	4096 Oct 29 14:36 shared/


### Database secrets

The simplest way is to use the regular `config/database.yml` but without having it in the git repository. If you checked it in, remove it with:

	~ git rm --cached config/database.yml

Then add the file to .gitignore and it will never be added to the repository. 

Update the `deploy.rb` file for Capistrano and add this line:

	append :linked_files, "config/database.yml"
	
This means that the application will look for a file in the directory `/var/www/your_app/shared/config` and will add a simlink to `/var/www/your_app/current`. 

Next ssh into your box and add the `database.yml` file into the directory `/var/www/your_app/shared/config`. 

## Letsencrypt SSL certificate

Now we need a SSL certificate for the server and domain. As an example I will use my [krx.pw](https://krx.pwd) domain. You need to follow exactly the steps shown in [this tutorial](https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal) for a Ubuntu 20 box.

Make also sure:

* to create a certificate for your_domain.com and also www.your_domain.com or any other subdomains for your_domain.com
* to configure your DNS correctly
* configure nginx correctly with setting `server_name your_domain.com www.your_domain.com`

That's it! 

FIN
	


	
	

































	

